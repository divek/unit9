# tasks

## Main setup
Will be done by terraform code. TF will also create the inventory file needed by next part of automation - for ansible. Creds.tf have the variables for AWS API

As I don't have experience with Debian on cloud I had to check of the AMIID. Pushed it into variables (know that it should be firstly filtered out by region via tf data).

Rest of customization of machine - creating the users, pushing keys, sudo, is done by ansible

## Automation
Some thought during writing:

- Inside install.yaml added also update of packages (there is also userdata in the terraform code, so basically no need to run it during installation), but as I saw instruction, I've added it tools

- Non-root account details:
User: check-user
pass: sha512crypt@#$89d6

Syslog is getting logs from dockerd, so the logs has to be manage further by it

Tags are in place. Should works. Also placed simple `test` to have possibility to return error if something happen inside container. Such test can be added also into CI/CD tools

#### Two question below automation subject
- APP_Secret has to be put into ansible vault, and recover from it during application start
- Monitoring: we have to monitor the OS itself (standard cloudwatch agent) and also application (super will be execute random GETs from another machine and check is still app running). Also horizontal scalability can be achived, but only if we try to manage also some lb or reverse proxy.

There was no specification where the image should be created, so it's created on the webserver node.

## Locust
Everything which was generated it's on /locust directory. Prepare test for 400 users in 5 minutes (300s), it means 1.3 users/s.
Don't have experience with analyse of such data. Probably `--threads 16 --workers 2` will help in such situation and scalling vertical and horizontal, but as I said before don't really know.


## CI/CD
In general, the build.yml it's already prepared into CI/CD app. If we change to code (here from devops branch), the container will be removed, the old image also, and next container will be populated. To do it in better way, blue-green deployment needs to be achived, via some loadbalancer and registration of containers on lb.

`Install.yml` and terraform could be also added into CI/CD tool if the project will growing. It could for sure speed up development.

Ahh, and ansible should be set on virtualenv (which I didn't create once started) - to rise up the version, because on docker_build I've got some depracated modules.

### PS

Why these tools?
- from last company expierience
- ansible and terraform is on heavy development
- ansible have a lot to pluses versus puppet (at least, I have fun). And better documentation
