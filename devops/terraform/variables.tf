variable "name-general" {
  description = "General name associated with objects"
  default     = "unit9"
}
variable "debian-ami-id" {
  description = "Debian AMI ID on eu-west-1"
  default     = "ami-0f65d545510d8b95d"
}

variable "key-ssh" {
  description = "Key used for ssh connection"
  default     = "rock64"
}
