provider "aws" {
  version = "~> 2.0"
  region  = "eu-west-1"
  access_key = var.access-key
  secret_key = var.secret-key
}

terraform {
  backend "s3" {
    bucket = "unit9-eu-west-1-tf-state"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}

# resource "aws_s3_bucket" "terraform_state" {
#   bucket = "${var.name-general}-eu-west-1-tf-state"
#   versioning {
#     enabled = true
#   }
#   lifecycle {
#     prevent_destroy = true
#   }
# }
#
resource "aws_instance" "web" {
  ami           = var.debian-ami-id
  instance_type = "t2.micro"
  security_groups = [ aws_security_group.allow-http-ssh.name ]
  key_name = var.key-ssh
  user_data = <<EOF
      #! /bin/bash
      sudo apt update
      sudo apt upgrade -y
EOF

  tags = {
    Workload = var.name-general
  }
}

resource "aws_security_group" "allow-http-ssh" {
  name        = "allow-http-ssh"
  description = "allow http and ssh"

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "egress open for world"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Workload = var.name-general
  }
}

data  "template_file" "inv" {
    template = "${file("./inventory.tpl")}"
    vars = {
        app_host = "${aws_instance.web.public_ip}"
    }
}

resource "local_file" "inv" {
  content  = data.template_file.inv.rendered
  filename = "../ansible/inventory"
}
