from flask import Flask, request
import os
import logging

app = Flask(__name__)

API_SECRET = os.getenv('API_SECRET', None)
if not API_SECRET:
    logging.error("API_SECRET env var not defined")
    exit(-1)

# let's enjoy some heavy load
def F(n):
    if n == 0: return 0
    elif n == 1: return 1
    else: return F(n-1)+F(n-2)


@app.route('/ok')
def ok():
    logging.info("It's all ok")
    return "it's ok", 200

@app.route('/error')
def error():
    logging.error("Houston we got a problem")
    return "nope", 500

@app.route('/api')
def api():
    if API_SECRET == request.args.get("API_SECRET"):
        logging.info("Secret is ok")
        return "it's ok", 200
    else:
        logging.error("Wrong secret!")
        return "not ok", 401

@app.route('/api/heavy')
def api_heavy():
    logging.info("computing hard")
    fib = F(30) - 831998
    logging.info("got it {}".format(fib))

    return "the answer of life the universe and everything is {}".format(fib), 200




@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=int(os.getenv('PORT', 8080)), debug=False, threaded=True)
